# KDE Akademy Workshop 2020

- 🌐 [Event](https://conf.kde.org/en/akademy2020/public/schedule/1)

## Content

- 💡 [Slides](https://labwork.dev/go/e1b78a)

## Repositories

- 🏗 [Source for Advanced Go CI Tests](https://gitlab.com/dnsmichi/kde-akademy-workshop-2020)

### History of exercises at KDE's infrastructure

- [CI/CD First Steps](https://invent.kde.org/dnsmichi/kde-workshop)
- [Advanced Go CI Tests](https://invent.kde.org/dnsmichi/kde-akademy-workshop-2020)

## Solutions

- `main.go`, `version.go` and `version_test.go` contain commented code used for CI/CD and test coverage exercises.

## Troubleshooting

- `.gitlab-ci.yml` needs an updated `REPO_NAME` for building Go with CI/CD
- `go.mod` might need a repository name update 

## Development

Initialize Go modules once.

```
go mod init
```

Download new dependencies during development.

```
go mod tidy
```
